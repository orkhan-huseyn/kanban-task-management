import { Button, Checkbox, TextArea, TextField } from './components';
import { Select } from './components/Select';

function App() {
  return (
    <>
      <h1>Button</h1>
      <Button large primary>
        Button Primary (L)
      </Button>
      <Button large danger>
        Button Destructive (L)
      </Button>
      <Button large>Button Secondary (L)</Button>
      <br /> <br />
      <Button primary>Button Primary (s)</Button>
      <Button danger>Button Primary (s)</Button>
      <Button>Button Secondary (s)</Button>
      <hr />
      <h1>Text field</h1>
      <div style={{ width: '450px', padding: '20px' }}>
        <TextField label="Task name" placeholder="Enter task name" />
        <br />
        <TextField
          isInvalid
          errorInfo="Can't be empty"
          placeholder="Enter task name"
        />
        <br />
        <TextArea placeholder="e.g. It’s always good to take a break. This 15 minute break will recharge the batteries a little." />
        <br />
        <Select label="Current Status">
          <option>Doing</option>
          <option>Completed</option>
          <option>In review</option>
        </Select>
      </div>
      <h1>Checkbox</h1>
      <div style={{ width: '350px', padding: '20px' }}>
        <Checkbox label="Do this crap" />
      </div>
    </>
  );
}

export default App;
