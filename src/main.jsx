import React from 'react';
import ReactDOM from 'react-dom/client';
import { createGlobalStyle } from 'styled-components';
import { normalize } from 'polished';
import App from './App';

const GlobalStyles = createGlobalStyle`
    ${normalize()}

    :root {
        --color-black: #000112;
        --color-very-dark-gray: #20212C;
        --color-dark-gray: #2B2C37;
        --color-lines: #3E3F4E;
        --color-medium-gray: #828FA3;
        --color-light-gray: #F4F7FD;
        --color-white: #FFFFFF;
        --color-main-purple: #635FC7;
        --color-main-purple-pale: #A8A4FF;
        --color-red: #EA5555;
        --color-red-pale: #FF9898;
    }

    * {
      box-sizing: border-box;
    }

    body {
        font-family: 'Plus Jakarta Sans', sans-serif;
        background-color: var(--color-light-gray);
        color:var(--color-black);
    }
`;

ReactDOM.createRoot(document.getElementById('root')).render(
  <>
    <GlobalStyles />
    <App />
  </>
);
