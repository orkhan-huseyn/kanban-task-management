import styled, { css } from 'styled-components';

export const Button = styled.button`
  cursor: pointer;
  border: none;
  border-radius: 20px;
  background: rgba(99, 95, 199, 0.1);
  color: var(--color-main-purple);
  padding: 9px 50px;
  text-align: center;
  font-size: 13px;
  line-height: 23px;
  font-weight: 700;
  transition: background 0.2s ease-in-out;
  outline: none;

  &:hover,
  &:focus {
    background: rgba(99, 95, 199, 0.25);
  }

  ${(props) =>
    props.large &&
    css`
      padding: 15px 50px;
      font-size: 15px;
      border-radius: 24px;
      line-height: 19px;
    `}

  ${(props) =>
    props.primary &&
    css`
      background: var(--color-main-purple);
      color: var(--color-white);

      &:hover,
      &:focus {
        background: var(--color-main-purple-pale);
      }
    `}

  ${(props) =>
    props.danger &&
    css`
      background: var(--color-red);
      color: var(--color-white);

      &:hover,
      &:focus {
        background: var(--color-red-pale);
      }
    `}
`;

