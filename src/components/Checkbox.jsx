import { useRef } from 'react';
import styled from 'styled-components';

const Input = styled.input`
  display: none;
`;

const Checkmark = styled.span`
  display: inline-block;
  background: var(--color-white);
  border: 2px solid rgba(130, 143, 163, 0.248914);
  border-radius: 2px;
  width: 16px;
  height: 16px;
  position: relative;

  &::after {
    content: '';
    display: none;
    position: absolute;
    left: 4px;
    top: 0;
    width: 5px;
    height: 10px;
    border: solid white;
    border-width: 0 3px 3px 0;
    transform: rotate(45deg);
  }
`;

const Text = styled.span`
  color: var(--color-black);
  font-size: 12px;
  font-weight: 700;
  line-height: 15px;
  margin-left: 16px;
`;

const Label = styled.label`
  cursor: pointer;
  display: flex;
  align-items: center;
  background: var(--color-light-gray);
  border-radius: 4px;
  border: none;
  padding: 12px;
  outline: none;

  &:hover,
  &:focus {
    background: rgba(99, 95, 199, 0.5);
  }

  ${Input}:checked ~ ${Checkmark} {
    background: var(--color-main-purple);
  }

  ${Input}:checked ~ ${Checkmark}::after {
    display: block;
  }

  ${Input}:checked ~ ${Text} {
    opacity: 0.5;
    text-decoration: line-through;
  }
`;

export function Checkbox({ label }) {
  const inputRef = useRef();

  function handleKeyDown(event) {
    if (event.key === ' ') {
      inputRef.current.click();
    }
  }

  return (
    <Label onKeyDown={handleKeyDown} tabIndex={0}>
      <Input ref={inputRef} type="checkbox" />
      <Checkmark />
      <Text>{label}</Text>
    </Label>
  );
}
