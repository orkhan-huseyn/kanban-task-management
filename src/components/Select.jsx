import { Input, Label, Wrapper, Info } from './TextField';

export function Select({ isInvalid, errorInfo, label, ...props }) {
  return (
    <>
      <Label visible={!!label} htmlFor={label}>
        {label}
      </Label>
      <Wrapper>
        <Input as="select" id={label} isInvalid={isInvalid} {...props} />
        {isInvalid && <Info>{errorInfo}</Info>}
      </Wrapper>
    </>
  );
}
