import styled, { css } from 'styled-components';

export const Input = styled.input`
  display: block;
  background: var(--color-white);
  color: var(--color-black);
  border: 2px solid rgba(130, 143, 163, 0.25);
  border-radius: 4px;
  padding: 10px 16px;
  outline: none;
  font-weight: 500;
  font-size: 13px;
  line-height: 23px;
  max-width: 100%;
  min-width: 100%;
  appearance: none;

  &::placeholder {
    color: var(--color-black);
    opacity: 0.25;
  }

  &:focus {
    border-color: var(--color-main-purple);
  }

  ${(props) =>
    props.isInvalid &&
    css`
      border-color: var(--color-red);
    `}
`;

export const Label = styled.label`
  display: inline-block;
  color: var(--color-medium-gray);
  margin-bottom: 8px;
  display: ${(props) => (props.visible ? 'block' : 'none')};
  font-size: 12px;
`;

export const Info = styled.span`
  display: inline-block;
  position: absolute;
  color: var(--color-red);
  top: 15px;
  right: 16px;
  font-size: 13px;
`;

export const Wrapper = styled.div`
  position: relative;
  min-height: 25px;
  display: block;
  width: 100%;
`;

export function TextField({ isInvalid, errorInfo, label, ...props }) {
  return (
    <>
      <Label visible={!!label} htmlFor={label}>
        {label}
      </Label>
      <Wrapper>
        <Input id={label} isInvalid={isInvalid} {...props} />
        {isInvalid && <Info>{errorInfo}</Info>}
      </Wrapper>
    </>
  );
}

export function TextArea({ isInvalid, errorInfo, label, ...props }) {
  return (
    <>
      <Label visible={!!label} htmlFor={label}>
        {label}
      </Label>
      <Wrapper>
        <Input as="textarea" rows={5} isInvalid={isInvalid} {...props} />
        {isInvalid && <Info>{errorInfo}</Info>}
      </Wrapper>
    </>
  );
}
